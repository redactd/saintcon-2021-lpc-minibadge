# LPC Minibadge.
<img src="/art/FINAL.png" alt="The LockPick Village Minibadge" width="200"/>

## Print your own
Grab the zip from the Gerbers folder to upload to a board house.

A stencil is recommended to make soldering the 0402 LEDs a lot easier.

## Parts List
- .011 Beryllium Copper Wire Springs: .136 O.D. X .325 length (look on eBay)
- Green 0402 LED (https://www.digikey.com/en/products/detail/w%C3%BCrth-elektronik/150040GS73240/8557155)
- 2K 0603 Resistor (https://www.digikey.com/en/products/detail/yageo/RC0603FR-072KL/727009)
- 2.54mm Connection Header (https://www.digikey.com/en/products/detail/sullins-connector-solutions/PREC002SAAN-RC/2774852)
